import React from 'react';
import "./Peoplescontacts.css";
import { Link } from "react-router-dom";
import apiConfig from "../../../apiConfig";
import img from "./../../../otherMaterials/Theoneste.jpg";

class Peopleslist extends React.Component {

    contact = {}
    constructor(props) {
        super(props);
    }
    
     // Returns the contacts in the http request
    
    fetchContact = () => {
        return fetch(`${apiConfig.apiAddress}/contacts/${this.props.match.params.id}`)
    }

    componentWillMount() {
        this.fetchContact()
            .then(resp => {
                return resp.json()
            }).then(resp => {
                console.log(resp);
                this.contact = resp;
                this.setState({ contact: resp });
            });
    }
    // 
    render() {
        
         //Fetch the contact that was given
         
        try {
            this.contact = this.state.contact;
        } catch (err) {

        }
        
        return (
            <div className="Peopleslist">
                
                {/* Go back at go back allow */}
                <div className="Peopleslist-Top">
                    <Link to="/" className="btn btn-sm btn-light">
                        <span className="mdi mdi-24px mdi-arrow-left"></span>
                    </Link>
                </div>
                

                <div className="spacer-v-md"></div>
                
                {/* Middle dection */}
                <div className="Peopleslist-Middle">
                    {/* Left part */}
                    <div className="Peopleslist-Middle-L">
                        <img src={img} alt=" " />
                    </div>
                    {/* Right part */}
                    <div className="Peopleslist-Middle-R">
                        {/*  */}
                        <div className="display-1" style={{ marginLeft: '15px', fontWeight: 'bold', textTransform: "capitalize" }}>
                            {this.contact.name}
                        </div>
                        <div className="Peopleslist-Middle-R-B">
                            <button className="btn"><span className="mdi mdi-message"></span></button>
                            <button className="btn"><span className="mdi mdi-phone"></span></button>
                            <button className="btn"><span className="mdi mdi-video"></span></button>
                            <button className="btn"><span className="mdi mdi-email"></span></button>
                        </div>
                    </div>
                </div>
               

                {/* Bottom section */}
                <div className="Peopleslist-Bottom">

                    {/* Phone */}
                    <div className="Peopleslist-Det">
                        <span className="mdi mdi-phone"></span>
                        <div className="Peopleslist-Det-A">
                            <div className="Peopleslist-Det-A-T"> {this.contact.phone_number}</div>
                            <div className="Peopleslist-Det-A-L">Mobile</div>
                        </div>
                    </div>

                    {/* Phone contact */}
                    <div className="Peopleslist-Det">
                        <span className="mdi mdi-home"></span>
                        <div className="Peopleslist-Det-A">
                            <div className="Peopleslist-Det-A-T">+250780850611</div>
                            <div className="Peopleslist-Det-A-L">Home</div>
                        </div>
                    </div>

                    {/* Email address */}
                    <div className="Peopleslist-Det">
                        <span className="mdi mdi-email"></span>
                        <div className="Peopleslist-Det-A">
                            <div className="Peopleslist-Det-A-T">theoneste99@gmail.com</div>
                            <div className="Peopleslist-Det-A-L">Email</div>
                        </div>
                    </div>
                    {/*  */}
                </div>
                {/*  */}
            </div>
        )
    }
}

export default Peopleslist;