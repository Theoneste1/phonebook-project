import React from 'react';
import "./Changeinfo.css";
import { Link, withRouter } from "react-router-dom";
import apiConfig from "../../../apiConfig";

// import Peopleslist from '../Peopleslist/Peopleslist';
class Peopleslist extends React.Component {

    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    apiAddress = "https://code-catalist-phone-book-rails.herokuapp.com";

    fetchContact = () => {
        console.log(`${this.apiAddress}/contacts/${this.props.match.params.id}`)
        return fetch(`${this.apiAddress}/contacts/${this.props.match.params.id}`)
    }


    componentDidMount() {
        if (this.isEdit) {
            this.fetchContact()
                .then(resp => {
                    return resp.json()
                }).then(resp => {
                    let contact = resp;
                    this.setState({ contact: resp });
                    // assign the data to form
                    let names = contact.name.split(" ");
                    let mainForm = document.getElementById("mainForm");
                    mainForm.fName.value = names.slice(0, names.length - 1);
                    mainForm.surName.value = names.slice(names.length - 1, names.length)
                    mainForm.phone_number.value = contact.phone_number

                });
        }
    }
    /**
     * @returns the current value of type of action
      */
    get type() {
        return this.props.type;
    }
    /**
     * @returns a boolean of either if it is in editing mode or not
     */
    get isEdit() {
        return this.type === 'edit';
    }

    goHome = () => {
        this.props.history.push("/")
    }
    /**
     * 
     * Handles the submission of the contact 
     * @param {*} event 
     */
    handleSubmit(event) {
        console.log(event.target)
        if (this.isEdit) {
            let data = {
                name: event.target.fName.value + " " + event.target.surName.value,
                phone_number: event.target.phone_number.value
            }
            fetch(`${apiConfig.apiAddress}/contacts/${this.props.match.params.id}`, {
                method: "PUT",
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then(resp => {
                    if (resp.ok) {
                        this.goHome()
                    }
                })
        } else {
            let data = {
                name: event.target.fName.value + " " + event.target.surName.value,
                phone_number: event.target.phone_number.value
            }
            console.log(data)
            fetch(`${apiConfig.apiAddress}/contacts`, {
                method: "POST",
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then(resp => {
                    console.log(resp)
                    if (resp.ok) {
                        this.goHome()
                    }
                })
        }
        event.preventDefault();

    }
    // 
    render() {
        /**
         * Fetch the contacts
         */
        try {
            this.contacts = this.state.contacts;
        } catch (err) {

        }
        /**
         * Conctacts
         */

        return (
            <div className="CForm">
                {/* Top section  */}
                {/* Go back */}
                <div className="CForm-Top">
                    <Link to="/" className="btn btn-sm btn-light">
                        <span className="mdi mdi-24px mdi-arrow-left"></span>
                    </Link>
                </div>
                {/* Middle dection */}
                <div className="CForm-Middle">
                    <form onSubmit={this.handleSubmit} id="mainForm" name="mainForm">
                        {/* Pic & Name SUrname */}
                        <div className="CForm-Aside">
                            <div className="CForm-Aside-Left">
                                <div className="placeholder">
                                    <span className="mdi mdi-36px mdi-camera"></span>
                                </div>
                            </div>
                            <div className="CForm-Aside-Right">
                                {/* name */}
                                <div className="input-group">
                                    <label htmlFor="fName" className="input-lbl">Name</label>
                                    <input id="fName" name="fName" className="input-box" placeholder="Theoneste" />
                                </div>
                                {/* surnName */}
                                <div className="input-group">
                                    <label htmlFor="surName" className="input-lbl">surnName</label>
                                    <input id="surName" name="surName" className="input-box" placeholder="Nsanzabarinda" />
                                </div>
                            </div>
                        </div>
                        {/* Mobile Nbr */}
                        <div>
                            {/* Mobile */}
                            <div className="input-group">
                                <label htmlFor="phone_number" className="input-lbl">Mobile</label>
                                <input id="phone_number" name="phone_number" className="input-box" placeholder="+250780850611" />
                            </div>
                        </div>
                        {/*  */}
                        {/*  */}
                        <div className="CForm-Bottom">
                            <button type="submit" className="btn btn-md btn-primary">
                                <span className={this.props.type == 'edit' ? 'mdi mdi-pencil' : 'mdi mdi-check'}></span>
                                <span>{this.props.type == 'edit' ? "Edit Contact" : "Save Contact"}</span>
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        )
    }
}

export default withRouter(Peopleslist);