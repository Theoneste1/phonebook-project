import React from 'react';
import './background.css';
import Bottom from "../../../components/navigation/Bottom";
import Toppart from "../../../components/navigation/Toppart";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";



// Importing Pages
import Peopleslist from '../Peopleslist/Peopleslist';
import Changeinfo from '../Changeinfo';
import ViewContact from "../Peoplescontacts/Peoplescontacts";
// 
import backgroundToppart from "./backgroundToppart";
// the compoment
export default function background() {

  /**
   * 
   */
  return (
    <div className="background">
      <Router>
        {/**
        * Top 
       */}
        <div className="background-Toppart">
          <Toppart />
        </div>
        {/*  
      Maain Container*/}
        <div className="background-Contents">
          <Switch>
            {/* Edit contact */}
            <Route path="/edit/:id">
              <backgroundToppart title="Edit Contact" />
              <Changeinfo type="edit" />
            </Route>
            {/*  */}
            {/* Add Contact */}
            <Route path="/add">
              <backgroundToppart title="Add Contact" />
              <Changeinfo type="add" />
            </Route>
            {/*  */}
            {/* View Contact */}
            <Route path={"/view/:id"} component={ViewContact}>
            </Route>
            {/*  */}
            {/* Contact lists */}
            <Route path="/">
              <backgroundToppart title="Contacts List" />
              <Peopleslist />
            </Route>

            {/*  */}
          </Switch>
        </div>
        {/*  */}
        {/* Bottom */}
        <div className="background-Bottom">
          <Bottom />
        </div>
      </Router>
    </div>
  );
}
