import React from 'react';

class backgroundToppart extends React.Component {

    render(){
        return (
            <div>
                <span className="display-1">{this.props.title}</span>
            </div>
        )
    }
}
export default backgroundToppart;