import React from 'react';
import "./Peopleslist.css";
import ContactAvatar from "./Avatarpeoples";
import { Link } from "react-router-dom";
import apiConfig from "../../../apiConfig";
import { withRouter } from "react-router-dom";

class Peopleslist extends React.Component {

    //passing through the contacts
    navigateToContact = (id) => {
        this.props.history.push(`/view/${id}`)
    }

    navigateToEditContact = (id) => {
        this.props.history.push(`/edit/${id}`)
    }

    contacts = [];
    
     // Returns the contacts in the http request
    
    fetchContacts = () => {
        return fetch(`${apiConfig.apiAddress}/contacts`)
    }
    fetchDeleteContact = (id) => {
        return fetch(`${apiConfig.apiAddress}/contacts/${id}`,{
            method: "delete"
        })
    }
    
     // Fetch the contacts fx
    
    deleteContact = (index) => {
        let contact = this.state.contacts[index];
        try {
            this.fetchDeleteContact(contact.id)
                .then(resp => {
                    // if (resp.ok) {
                        this.contacts = this.state.contacts;
                        // let contacts = [];
                        this.contacts.splice(index, 1);
                        this.setState({ contacts: this.contacts })
                    // }
                })
        } catch (err) {

        }
    }



    componentDidMount() {
        this.fetchContacts()
            .then(resp => {
                return resp.json()
            }).then(resp => {
                if (resp instanceof Array) {
                    this.contacts = resp;
                    this.setState({ contacts: resp });
                }
            });
    }
    // 
    render() {
        
         //Fetch the contacts
         
        try {
            this.contacts = this.state.contacts;
        } catch (err) {

        }
        
         // Conctacts
         

        return (
            <div>
                {/* The table section */}
                <div className="space-v-md"></div>

                
                <div>
                    <table className="Phone-Table">
                        
                        <thead>
                            <tr>
                                <td>NAME</td>
                                <td>PHONE NUMBER</td>
                                <td>EDIT</td>
                                <td>DELETE</td>
                            </tr>
                        </thead>
                        
                        <tbody>
                            
                            {
                                this.contacts.map((contact, index) => {
                                    return (
                                        <tr key={contact.id}>
                                            <td onClick={this.navigateToContact.bind(this, contact.id)}>

                                                {/* <PeopleAvatar name={contact.name} /> */}

                                                <span className="txt-primary Account-Image mdi mdi-36px mdi-account-circle"></span>
                                                <span>{contact.name}</span>
                                            </td>
                                            <td>{contact.phone_number}</td>
                                            <td>
                                                <button className="btn" onClick={this.navigateToEditContact.bind(this, contact.id)}>Edit</button>
                                            </td>
                                            <td>
                                                <button className="btn" onClick={this.deleteContact.bind(this, index)}>Delete</button>
                                            </td>
                                        </tr>
                                    )
                                }
                                )}
                            
                        </tbody>
                    </table>
                </div>
                
                <div className="space-v-md"></div>
                {/* The add contact button */}
                <div>
                    <Link to="add" className="btn btn-md btn-primary">
                        <span className="mdi mdi-plus"></span>
                        <span>ADD CONTACT</span>
                    </Link>

                </div>
            </div>
        )
    }
}

export default withRouter(Peopleslist);