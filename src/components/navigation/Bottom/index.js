import React from 'react';
import './Bottom.css';

export default function () {
    let year = new Date().getFullYear();
    return (
        <div className="Bottom">
            <span>All rights reserved @ {year}</span>
        </div>
    );
}