import React from 'react';
import './Toppart.css';
import Logo from "./../../../otherMaterials/logo.svg";
import AccountDrop from "./AccountDrop";
export default function () {
    let account = {
        name: "Admin Theoneste"
    }

    return (
        <div className="Header">
            <div className="Header-Containter">
                <div className="Header-Containter-H">
                    {/* Logo  */}
                    
                    <div className="Header-Logo">
                        <img src="https://image.flaticon.com/icons/png/512/124/124010.png" alt="Logo" />
                    </div>
                    {/*  */}
                    <div className="Header-Dropdown">
                        <AccountDrop account={account} />
                    </div>
                    {/*  */}
                </div>
            </div>
        </div>
    );
}