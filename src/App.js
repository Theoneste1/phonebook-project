import React from 'react';
import './App.css';
import MainPage from "./pages/Main/background/background";
function App() {
  return (
    <div className="App">
      <MainPage/>
    </div>
  );
}

export default App;
